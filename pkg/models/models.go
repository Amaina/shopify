package models

import (
	"errors"
)

//ErrNoRecord is the error we return when the record doesn't exist
var ErrNoRecord = errors.New("models: no matching record found")

//Product struct is used to store information about a product
type Product struct {
	ID              int
	Name            string
	Description     string
	ActualPrice     int
	DiscountedPrice int
	SubCategory     string
	SalesCount      int
	Image           string
}

//MainCategories struct is used to store information about categories
type MainCategories struct {
	ID           int
	CategoryName string
	Description  string
	Image        string
}

//Response struct stores the status of uploads etc
type Response struct {
	Status          string
	Filename        string
	Description     string
	CategoryName    string
	SubCategoryName string
	ProductName     string
	SubCategoryID   int
	MainCategoryID  int
}

// SubCategories struct stores all subcategories
type SubCategories struct {
	ID               int
	SubCategoryName  string
	MainCategoryName string
	MainCategoryID   string
	Description      string
	Image            string
}

//Subs stores the category Id we used to retrieve the sub categories
type Subs struct {
	SubCategory string
	ID          int
}

// CatJSON returns the subcategories of a given category ID
type CatJSON struct {
	Category      string                   `json:"Category"`
	SubCategories []map[string]interface{} `json:"SubCategories"`
}
