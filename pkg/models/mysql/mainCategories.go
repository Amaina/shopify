package mysql

import (
	"database/sql"
	"encoding/json"
	"strconv"

	"gitlab.com/amaish/shopify/pkg/models"
)

//MainCategoriesModel is used to get or insert data to the DB
type MainCategoriesModel struct {
	DB *sql.DB
}

//Insert is used to write to our DB
func (m *MainCategoriesModel) Insert(categoryName, description, image string) (int, error) {

	stmt := `INSERT INTO mainCategories (categoryName, description, image) VALUES (?, ?, ?)`
	result, err := m.DB.Exec(stmt, categoryName, description, image)
	if err != nil {
		return 0, err
	}
	// id, err := result.LastInsertId()
	// if err != nil {
	// 	return 0, err
	// }

	// return int(id), nil //uncomment if you need to return id

	_, err = result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return 0, nil

}

//Get is used to retrieve a specific product
func (m *MainCategoriesModel) Get(id int) (*models.MainCategories, error) {
	stmt := `SELECT id, categoryName, description, image FROM mainCategories WHERE id = ?`
	row := m.DB.QueryRow(stmt, id)

	cat := &models.MainCategories{}
	err := row.Scan(&cat.ID, &cat.CategoryName, &cat.Description, &cat.Image)
	if err == sql.ErrNoRows {
		return nil, models.ErrNoRecord
	} else if err != nil {
		return nil, err
	}
	return cat, nil
}

//All will retrieve the top selling items
func (m *MainCategoriesModel) All() ([]*models.MainCategories, error) {
	stmt := `SELECT id, categoryName, description, image FROM mainCategories`
	rows, err := m.DB.Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	mainCategories := []*models.MainCategories{}
	for rows.Next() {
		cat := &models.MainCategories{}
		err := rows.Scan(&cat.ID, &cat.CategoryName, &cat.Description, &cat.Image)
		if err != nil {
			return nil, err
		}

		mainCategories = append(mainCategories, cat)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return mainCategories, nil
}

//AllCategoriesWithSub will retrieve all categories and the sub categories
func (m *MainCategoriesModel) AllCategoriesWithSub() ([]byte, error) { //[]*models.SubCatM add return
	stmt := `SELECT id, categoryName, description, image FROM mainCategories`
	rows, err := m.DB.Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	mainCategories := []*models.MainCategories{}
	var catDataRaw []interface{}
	catJSON := models.CatJSON{}
	for rows.Next() {

		cat := &models.MainCategories{}
		err := rows.Scan(&cat.ID, &cat.CategoryName, &cat.Description, &cat.Image)
		if err != nil {
			return nil, err
		}

		stmt := `SELECT id, subCategoryName FROM subCategories WHERE mainCategoryId = ?`
		subs, err := m.DB.Query(stmt, cat.ID)
		if err != nil {
			return nil, err
		}
		defer subs.Close()
		var subCategories []map[string]interface{}
		for subs.Next() {
			subCat := models.Subs{}
			err := subs.Scan(&subCat.ID, &subCat.SubCategory)
			if err != nil {
				return nil, err
			}

			// var subCatWithID map[string]interface{}

			// subCatWithID = "Id" + strconv.Itoa(subCat.ID) + "Name " + subCat.SubCategory
			subCatWithID := map[string]interface{}{
				"Id":   strconv.Itoa(subCat.ID),
				"Name": subCat.SubCategory,
			}

			subCategories = append(subCategories, subCatWithID)
		}

		catJSON.Category = cat.CategoryName
		catJSON.SubCategories = subCategories

		mainCategories = append(mainCategories, cat)

		catDataRaw = append(catDataRaw, catJSON)

	}
	catDataJSON, err := json.MarshalIndent(catDataRaw, "", "\t")
	if err != nil {
		return nil, err
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return catDataJSON, nil
}
