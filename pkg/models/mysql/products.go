package mysql

import (
	"database/sql"

	"gitlab.com/amaish/shopify/pkg/models"
)

//ProductModel is used to get or insert data to the DB
type ProductModel struct {
	DB *sql.DB
}

//Insert is used to write to our DB
func (m *ProductModel) Insert(name, description string, actualPrice, discountedPrice int, mainCategory, subCategory string, subCategoryID int, image string) (int, error) {

	stmt := `INSERT INTO products (name, description, actualPrice, discountedPrice, mainCategory, subCategory, subCategoryId, image) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`
	result, err := m.DB.Exec(stmt, name, description, actualPrice, discountedPrice, mainCategory, subCategory, subCategoryID, image)
	if err != nil {
		return 0, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil

}

//Get is used to retrieve a specific product
func (m *ProductModel) Get(id int) (*models.Product, error) {
	stmt := `SELECT id, name, description, actualPrice, discountedPrice, salesCount, image FROM products WHERE id = ?`
	row := m.DB.QueryRow(stmt, id)

	p := &models.Product{}
	err := row.Scan(&p.ID, &p.Name, &p.Description, &p.ActualPrice, &p.DiscountedPrice, &p.SalesCount, &p.Image)
	if err == sql.ErrNoRows {
		return nil, models.ErrNoRecord
	} else if err != nil {
		return nil, err
	}
	return p, nil
}

//Top will retrieve the top selling items
func (m *ProductModel) Top() ([]*models.Product, error) {
	stmt := `SELECT id, name, description, actualPrice, discountedPrice, salesCount, image FROM products ORDER BY salesCount DESC LIMIT 5`
	rows, err := m.DB.Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	products := []*models.Product{}
	for rows.Next() {
		p := &models.Product{}
		err := rows.Scan(&p.ID, &p.Name, &p.Description, &p.ActualPrice, &p.DiscountedPrice, &p.SalesCount, &p.Image)
		if err != nil {
			return nil, err
		}

		products = append(products, p)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return products, nil
}

// GetAll gets all the products of a specific subCategory
func (m *ProductModel) GetAll(id int) ([]*models.Product, error) {
	stmt := `SELECT id, name, description, actualPrice, discountedPrice, subCategory, salesCount, image FROM products WHERE subCategoryId = ?`
	rows, err := m.DB.Query(stmt, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	products := []*models.Product{}
	for rows.Next() {
		p := &models.Product{}
		err := rows.Scan(&p.ID, &p.Name, &p.Description, &p.ActualPrice, &p.DiscountedPrice, &p.SubCategory, &p.SalesCount, &p.Image)
		if err != nil {
			return nil, err
		}

		products = append(products, p)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return products, nil
}
