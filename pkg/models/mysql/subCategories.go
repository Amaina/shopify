package mysql

import (
	"database/sql"

	"gitlab.com/amaish/shopify/pkg/models"
)

//SubCategoriesModel creates the db connection for data processing
type SubCategoriesModel struct {
	DB *sql.DB
}

//Insert is used to write to our DB
func (m *SubCategoriesModel) Insert(subCategoryName, mainCategoryName string) (int, error) {

	stmt := `INSERT INTO subCategories (subCategoryName, mainCategoryName) VALUES (?, ?)`
	result, err := m.DB.Exec(stmt, subCategoryName, mainCategoryName)
	if err != nil {
		return 0, err
	}
	// id, err := result.LastInsertId()
	// if err != nil {
	// 	return 0, err
	// }

	// return int(id), nil //uncomment if you need to return id

	_, err = result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return 0, nil

}

//Get is used to retrieve a specific subCategory
func (m *SubCategoriesModel) Get(id int) (*models.SubCategories, error) {
	stmt := `SELECT id, subCategoryName, mainCategoryName FROM subCategories WHERE id = ?`
	row := m.DB.QueryRow(stmt, id)

	cat := &models.SubCategories{}
	err := row.Scan(&cat.ID, &cat.SubCategoryName, &cat.MainCategoryName)
	if err == sql.ErrNoRows {
		return nil, models.ErrNoRecord
	} else if err != nil {
		return nil, err
	}
	return cat, nil
}

//AllSubCategories will retrieve all subCategories of a specific category
func (m *SubCategoriesModel) AllSubCategories(id int) ([]*models.SubCategories, error) {
	stmt := `SELECT id, subCategoryName, mainCategoryName, description, image FROM subCategories WHERE mainCategoryId = ?`
	rows, err := m.DB.Query(stmt, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	subCategories := []*models.SubCategories{}
	for rows.Next() {
		cat := &models.SubCategories{}
		err := rows.Scan(&cat.ID, &cat.SubCategoryName, &cat.MainCategoryName, &cat.Description, &cat.Image)
		if err != nil {
			return nil, err
		}

		subCategories = append(subCategories, cat)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return subCategories, nil
}
