module gitlab.com/amaish/shopify

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/justinas/alice v1.2.0
)
