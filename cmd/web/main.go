package main

import (
	"database/sql"
	"flag"
	"html/template"
	"log"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/amaish/shopify/pkg/models/mysql"
)

type application struct {
	errorLog       *log.Logger
	infoLog        *log.Logger
	products       *mysql.ProductModel
	mainCategories *mysql.MainCategoriesModel
	subCategories  *mysql.SubCategoriesModel
	templateCache  map[string]*template.Template
}

func main() {
	addr := flag.String("addr", ":4000", "HTTP network address")
	dsn := flag.String("dsn", "root:123@aMn@1982@/shopify?parseTime=true", "MySQL data source name")
	flag.Parse()

	// errorLog := log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
	appErrorLog, err := os.OpenFile("./logs/error.log", os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer appErrorLog.Close()

	errorLog := log.New(appErrorLog, "ERROR\t", log.Ldate|log.Ltime)

	db, err := openDB(*dsn)
	if err != nil {
		log.Println("An error has occured")
		errorLog.Fatal(err)
	}

	defer db.Close()

	templateCache, err := newTemplateCache("./ui/html")
	if err != nil {
		errorLog.Fatal(err)
	}

	appLog, err := os.OpenFile("./logs/info.log", os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer appLog.Close()
	infoLog := log.New(appLog, "INFO\t", log.Ldate|log.Ltime)
	log.Println("Server started successfully")

	app := &application{
		errorLog:       errorLog,
		infoLog:        infoLog,
		products:       &mysql.ProductModel{DB: db},
		mainCategories: &mysql.MainCategoriesModel{DB: db},
		subCategories:  &mysql.SubCategoriesModel{DB: db},
		templateCache:  templateCache,
	}

	srv := &http.Server{
		Addr:     *addr,
		ErrorLog: errorLog,
		Handler:  app.routes(),
	}

	infoLog.Printf("Starting server on %s", *addr)

	err = srv.ListenAndServe()
	log.Println("An error has occured")
	errorLog.Fatal(err)
}

func openDB(dsn string) (*sql.DB, error) {
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return db, nil
}
