package main

import (
	"net/http"

	"github.com/justinas/alice"
)

func (app *application) routes() http.Handler {

	standardMiddleware := alice.New(app.recoverPanic, app.logRequest, secureHeaders)

	mux := http.NewServeMux()
	mux.HandleFunc("/", app.home)
	mux.HandleFunc("/categories", app.categories)
	mux.HandleFunc("/productList", app.productList)
	mux.HandleFunc("/productSmallList", app.productSmallList)
	mux.HandleFunc("/product", app.product)
	mux.HandleFunc("/cart", app.cart)
	mux.HandleFunc("/checkoutSign", app.checkoutSign)
	mux.HandleFunc("/checkoutShipping", app.checkoutShipping)
	mux.HandleFunc("/checkoutPayment", app.checkoutPayment)
	mux.HandleFunc("/confirmPayment", app.confirmPayment)
	mux.HandleFunc("/trackingOrder", app.trackingOrder)
	mux.HandleFunc("/panelAccount", app.panelAccount)
	mux.HandleFunc("/formAccount", app.formAccount)
	mux.HandleFunc("/orderHistory", app.orderHistory)
	mux.HandleFunc("/blogList", app.blogList)
	mux.HandleFunc("/blogInside", app.blogInside)
	mux.HandleFunc("/standardPage", app.standardPage)
	mux.HandleFunc("/faq", app.faq)
	mux.HandleFunc("/stylingGuide", app.stylingGuide)
	mux.HandleFunc("/shortcodes", app.shortcodes)
	mux.HandleFunc("/contact", app.contact)
	mux.HandleFunc("/showProduct", app.showProduct)
	mux.HandleFunc("/addProduct", app.addProduct)
	mux.HandleFunc("/productAdd", app.productAdd)
	mux.HandleFunc("/categoryAdd", app.categoryAdd)
	mux.HandleFunc("/addCategory", app.addCategory)
	mux.HandleFunc("/subCategoryProducts", app.subCategoryProducts)
	mux.HandleFunc("/subCategories", app.showSubCategories)
	mux.HandleFunc("/addSubCategory", app.addSubCategory)
	mux.HandleFunc("/subCategoryAdd", app.subCategoryAdd)
	mux.HandleFunc("/index", app.index)

	//place the handle funcs that serve the html here

	fileServer := http.FileServer(http.Dir("./ui/static/"))
	mux.Handle("/static/", http.StripPrefix("/static", fileServer))

	return standardMiddleware.Then(mux)
}
