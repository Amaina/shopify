package main

import (
	"html/template"
	"path/filepath"

	"gitlab.com/amaish/shopify/pkg/models"
)

type templateData struct {
	CurrentYear    int
	Product        *models.Product
	Products       []*models.Product
	MainCategories []*models.MainCategories
	SubCategories  []*models.SubCategories
	Response       models.Response
	CatDataJSON    []models.CatJSON
	JSONstring     string
	// SubCat         []*models.SubCat
}

func newTemplateCache(dir string) (map[string]*template.Template, error) {
	cache := map[string]*template.Template{}
	pages, err := filepath.Glob(filepath.Join(dir, "*.page.tmpl"))
	if err != nil {
		return nil, err
	}

	for _, page := range pages {
		name := filepath.Base(page)

		ts, err := template.ParseFiles(page)
		if err != nil {
			return nil, err
		}

		ts, err = ts.ParseGlob(filepath.Join(dir, "*.layout.tmpl"))
		if err != nil {
			return nil, err
		}

		ts, err = ts.ParseGlob(filepath.Join(dir, "*.partial.tmpl"))
		if err != nil {
			return nil, err
		}

		cache[name] = ts
	}

	return cache, nil
}
