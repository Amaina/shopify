package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/amaish/shopify/pkg/models"
)

func (app *application) home(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "home.page.tmpl", data)

}

func (app *application) categories(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/categories" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "category.page.tmpl", data)

}

func (app *application) productList(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/productList" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "product-list.page.tmpl", data)

}

func (app *application) productSmallList(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/productSmallList" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "product-small-list.page.tmpl", data)

}

func (app *application) product(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/product" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "product.page.tmpl", data)

}

func (app *application) cart(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/cart" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "cart.page.tmpl", data)

}

func (app *application) checkoutSign(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/checkoutSign" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "checkout-sign.page.tmpl", data)

}

func (app *application) checkoutPayment(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/checkoutPayment" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "checkout-payment.page.tmpl", data)

}

func (app *application) checkoutShipping(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/checkoutShipping" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "checkout-shipping.page.tmpl", data)

}

func (app *application) confirmPayment(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/confirmPayment" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "payment-confirm.page.tmpl", data)

}

func (app *application) trackingOrder(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/trackingOrder" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "tracking-order.page.tmpl", data)

}

func (app *application) panelAccount(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/panelAccount" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "panel-account.page.tmpl", data)

}

func (app *application) formAccount(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/formAccount" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "form-account.page.tmpl", data)

}

func (app *application) orderHistory(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/orderHistory" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}
	app.render(w, r, "order-history.page.tmpl", data)

}

func (app *application) blogInside(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/blogInside" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "blog-inside.page.tmpl", data)

}

func (app *application) blogList(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/blogList" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "blog-list.page.tmpl", data)

}

func (app *application) standardPage(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/standardPage" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "standard-page.page.tmpl", data)

}

func (app *application) faq(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/faq" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "faq.page.tmpl", data)

}

func (app *application) stylingGuide(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/stylingGuide" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "styling-guide.page.tmpl", data)

}

func (app *application) shortcodes(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/shortcodes" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "shortcodes.page.tmpl", data)

}

func (app *application) contact(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/contact" {
		app.notFound(w)
		return
	}

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub}

	app.render(w, r, "contact.page.tmpl", data)

}

func (app *application) showProduct(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	p, err := app.products.Get(id)
	if err == models.ErrNoRecord {
		app.notFound(w)
		return
	} else if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Product: p, CatDataJSON: catWithSub}

	app.render(w, r, "showProduct.page.tmpl", data)

}

func (app *application) addCategory(w http.ResponseWriter, r *http.Request) {

	// if r.URL.Path != "/addCategory" {
	// 	app.notFound(w)
	// 	return
	// }

	p, err := app.products.Top()
	if err != nil {
		app.serverError(w, err)
		return
	}

	status := r.URL.Query().Get("status")
	filename := r.URL.Query().Get("filename")
	description := r.URL.Query().Get("description")
	categoryname := r.URL.Query().Get("categoryname")

	response := models.Response{
		Status:       status,
		Filename:     filename,
		Description:  description,
		CategoryName: categoryname,
	}

	cat, err := app.mainCategories.All()
	if err != nil {
		app.serverError(w, err)
		return
	}

	mainCat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(mainCat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := &templateData{Products: p, MainCategories: cat, CatDataJSON: catWithSub, Response: response}

	app.render(w, r, "addCategory.page.tmpl", data)

}

func (app *application) categoryAdd(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.Header().Set("Allow", "POST")
		app.clientError(w, http.StatusMethodNotAllowed)
		return
	}
	r.ParseMultipartForm(10 << 20)

	file, handler, err := r.FormFile("image")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	// buf := bytes.NewBuffer(nil)
	// if _, err = io.Copy(buf, file); err != nil {
	// 	log.Fatal(err)
	// }
	// nameParts := strings.Split(handler.Filename, ".")

	// _, err = app.mainCategories.Insert(strings.Join(r.Form["categoryName"], " "), strings.Join(r.Form["description"], " "), handler.Filename)

	// if err != nil {
	// 	app.serverError(w, err)
	// 	return
	// }
	// err = ioutil.WriteFile("./ui/static/images/"+nameParts[0]+"."+nameParts[1], buf.Bytes(), 0777)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	status := "Successfully Added"
	filename := handler.Filename
	description := strings.Join(r.Form["description"], " ")
	categoryname := strings.Join(r.Form["categoryName"], " ")

	http.Redirect(w, r, fmt.Sprintf("/addCategory?status=%s&filename=%s&description=%s&categoryname=%s", status, filename, description, categoryname), http.StatusSeeOther)

}

func (app *application) subCategoryProducts(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	p, err := app.products.GetAll(id)
	if err == models.ErrNoRecord {
		app.notFound(w)
		return
	} else if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(cat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	subcategoryname := r.URL.Query().Get("subCategory")
	categoryname := r.URL.Query().Get("category")

	response := models.Response{
		SubCategoryName: subcategoryname,
		SubCategoryID:   id,
		CategoryName:    categoryname,
	}

	data := &templateData{Products: p, CatDataJSON: catWithSub, Response: response}

	app.render(w, r, "subCategoryProducts.page.tmpl", data)

}

func (app *application) addProduct(w http.ResponseWriter, r *http.Request) {

	status := r.URL.Query().Get("status")
	productName := r.URL.Query().Get("productName")
	description := r.URL.Query().Get("description")
	image := r.URL.Query().Get("filename")
	categoryName := r.URL.Query().Get("categoryName")
	subCategoryName := r.URL.Query().Get("subCategoryName")
	subCategoryID, _ := strconv.Atoi(r.URL.Query().Get("subCategoryId"))

	response := models.Response{
		Status:          status,
		ProductName:     productName,
		Description:     description,
		Filename:        image,
		CategoryName:    categoryName,
		SubCategoryName: subCategoryName,
		SubCategoryID:   subCategoryID,
	}

	data := &templateData{Response: response}

	app.render(w, r, "addProduct.page.tmpl", data)
}

func (app *application) productAdd(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.Header().Set("Allow", "POST")
		app.clientError(w, http.StatusMethodNotAllowed)
		return
	}
	r.ParseMultipartForm(10 << 20)

	file, handler, err := r.FormFile("image")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	// buf := bytes.NewBuffer(nil)
	// if _, err = io.Copy(buf, file); err != nil {
	// 	log.Fatal(err)
	// }
	// nameParts := strings.Split(handler.Filename, ".")

	actualPriceStirng := strings.Join(r.Form["actualPrice"], " ")
	discountedPricestring := strings.Join(r.Form["discountedPrice"], " ")
	subCategoryID := r.URL.Query().Get("subCategoryId")
	// subCategoryID, err := strconv.Atoi(r.URL.Query().Get("subCategoryId"))

	// if err != nil {
	// 	app.serverError(w, err)
	// 	return
	// }
	subCategoryName := r.URL.Query().Get("subCategoryName")
	categoryName := r.URL.Query().Get("categoryName")

	// actualPrice, err := strconv.Atoi(actualPriceStirng)

	// if err != nil {
	// 	app.serverError(w, err)
	// 	return
	// }

	// discountedPrice, err := strconv.Atoi(discountedPricestring)

	// if err != nil {
	// 	app.serverError(w, err)
	// 	return
	// }

	// _, err = app.products.Insert(strings.Join(r.Form["productName"], " "), strings.Join(r.Form["description"], " "), actualPrice, discountedPrice, categoryName, subCategoryName, subCategoryID, handler.Filename)

	// if err != nil {
	// 	app.serverError(w, err)
	// 	return
	// }
	// err = ioutil.WriteFile("./ui/static/images/products/"+nameParts[0]+"."+nameParts[1], buf.Bytes(), 0777)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	status := "Successfully Added"
	filename := handler.Filename

	http.Redirect(w, r, fmt.Sprintf("/addProduct?productName=%s&status=%s&filename=%s&actualPrice=%s&discountedPrice=%s&categoryName=%s&subCategoryId=%s&subCategoryName=%s", strings.Join(r.Form["productName"], ""), status, filename, actualPriceStirng, discountedPricestring, categoryName, subCategoryID, subCategoryName), http.StatusSeeOther)
}

func (app *application) showSubCategories(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(r.URL.Query().Get("mainCategoryId"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	p, err := app.subCategories.AllSubCategories(id)
	if err == models.ErrNoRecord {
		app.notFound(w)
		return
	} else if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(cat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	categoryname := r.URL.Query().Get("name")

	response := models.Response{
		CategoryName:   categoryname,
		MainCategoryID: id,
	}

	data := &templateData{SubCategories: p, CatDataJSON: catWithSub, Response: response}

	app.render(w, r, "subCategories.page.tmpl", data)

}

func (app *application) addSubCategory(w http.ResponseWriter, r *http.Request) {

	status := r.URL.Query().Get("status")
	subCategoryName := r.URL.Query().Get("subCategoryName")
	mainCategoryName := r.URL.Query().Get("mainCategoryName")
	description := r.URL.Query().Get("description")
	image := r.URL.Query().Get("filename")
	mainCategoryID, _ := strconv.Atoi(r.URL.Query().Get("mainCategoryId"))

	response := models.Response{
		Status:          status,
		Description:     description,
		Filename:        image,
		CategoryName:    mainCategoryName,
		SubCategoryName: subCategoryName,
		MainCategoryID:  mainCategoryID,
	}

	data := &templateData{Response: response}

	app.render(w, r, "addSubcategory.page.tmpl", data)
}

func (app *application) subCategoryAdd(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.Header().Set("Allow", "POST")
		app.clientError(w, http.StatusMethodNotAllowed)
		return
	}
	r.ParseMultipartForm(10 << 20)

	file, handler, err := r.FormFile("image")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	// buf := bytes.NewBuffer(nil)
	// if _, err = io.Copy(buf, file); err != nil {
	// 	log.Fatal(err)
	// }
	// nameParts := strings.Split(handler.Filename, ".")

	subCategoryName := strings.Join(r.Form["subCategoryName"], " ")
	description := strings.Join(r.Form["description"], " ")
	mainCategoryID := r.URL.Query().Get("mainCategoryId")
	// subCategoryID, err := strconv.Atoi(r.URL.Query().Get("subCategoryId"))

	// if err != nil {
	// 	app.serverError(w, err)
	// 	return
	// }
	categoryName := r.URL.Query().Get("categoryName")

	// actualPrice, err := strconv.Atoi(actualPriceStirng)

	// if err != nil {
	// 	app.serverError(w, err)
	// 	return
	// }

	// discountedPrice, err := strconv.Atoi(discountedPricestring)

	// if err != nil {
	// 	app.serverError(w, err)
	// 	return
	// }

	// _, err = app.products.Insert(strings.Join(r.Form["productName"], " "), strings.Join(r.Form["description"], " "), actualPrice, discountedPrice, categoryName, subCategoryName, subCategoryID, handler.Filename)

	// if err != nil {
	// 	app.serverError(w, err)
	// 	return
	// }
	// err = ioutil.WriteFile("./ui/static/images/subCategories/"+nameParts[0]+"."+nameParts[1], buf.Bytes(), 0777)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	status := "Successfully Added"
	filename := handler.Filename

	http.Redirect(w, r, fmt.Sprintf("/addSubCategory?subCategoryName=%s&status=%s&filename=%s&categoryName=%s&mainCategoryID=%s&description=%s", subCategoryName, status, filename, categoryName, mainCategoryID, description), http.StatusSeeOther)
}

func (app *application) index(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	p, err := app.subCategories.AllSubCategories(id)
	if err == models.ErrNoRecord {
		app.notFound(w)
		return
	} else if err != nil {
		app.serverError(w, err)
		return
	}

	cat, err := app.mainCategories.AllCategoriesWithSub()
	if err != nil {
		app.serverError(w, err)
		return
	}

	var catWithSub []models.CatJSON

	err = json.Unmarshal(cat, &catWithSub)
	if err != nil {
		app.serverError(w, err)
		return
	}

	subcategoryname := r.URL.Query().Get("name")

	response := models.Response{
		SubCategoryName: subcategoryname,
	}

	data := &templateData{SubCategories: p, CatDataJSON: catWithSub, Response: response}

	app.render(w, r, "index.page.tmpl", data)

}
